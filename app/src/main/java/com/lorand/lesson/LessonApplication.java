package com.lorand.lesson;

import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

import com.lorand.lesson.bl.KeepAliveService;

/**
 * Created by n0sferat0k on 1/3/2016.
 */
public class LessonApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, getString(R.string.im_alive), Toast.LENGTH_SHORT).show();

        startService(new Intent(this, KeepAliveService.class));
    }
}
