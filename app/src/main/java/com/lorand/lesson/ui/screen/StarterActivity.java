package com.lorand.lesson.ui.screen;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lorand.lesson.R;
import com.lorand.lesson.ui.fragment.HomeRemoteFragment;
import com.lorand.lesson.ui.fragment.RcFragment;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class StarterActivity extends Activity {
    private static final int RC_MENU_POSITION = 0;
    private static final int HOME_REMOTE_MENU_POSITION = 1;

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.starter_activity);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.navigation_drawer_item);
        adapter.addAll(getResources().getStringArray(R.array.navigation_drawer_items_array));
        drawerList.setAdapter(adapter);
        drawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    private void selectItem(int position) {
        Fragment fragment = null;
        String tag = null;
        switch (position) {
            case RC_MENU_POSITION:
                tag = RcFragment.TAG;
                fragment = new RcFragment();
                break;
            case HOME_REMOTE_MENU_POSITION:
                tag = HomeRemoteFragment.TAG;
                fragment = new HomeRemoteFragment();
                break;
        }
        if(fragment != null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, fragment, tag).commit();
        }
        drawerLayout.closeDrawers();
    }
}
