package com.lorand.lesson.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lorand.lesson.R;
import com.lorand.lesson.bean.LogItem;
import com.lorand.lesson.bl.CarController;
import com.lorand.lesson.ui.adapter.MoveListAdapter;

import java.util.ArrayList;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class MoveListFragment extends Fragment {
    private static final String KEY_LOG_ITEM_LIST = "log.item.list";

    private ListView moveList;
    private MoveListAdapter moveListAdapter;

    public MoveListFragment() {
        setRetainInstance(true);
        moveListAdapter = new MoveListAdapter();
    }

    public void setCarController(CarController carController) {
        carController.setCommandLogger(moveListAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.move_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        moveList = (ListView) getView().findViewById(R.id.move_list);
        moveList.setAdapter(moveListAdapter);

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_LOG_ITEM_LIST)) {
            ArrayList<LogItem> logItems = savedInstanceState.getParcelableArrayList(KEY_LOG_ITEM_LIST);
            moveListAdapter.setLogItems(logItems);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_LOG_ITEM_LIST, moveListAdapter.getLogItems());
    }
}
