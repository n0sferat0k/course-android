package com.lorand.lesson.ui.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AnimationCluster implements Animator.AnimatorListener, Interpolator {

    private final Handler mainHandler;
    private AnimatorSet animatorSet = new AnimatorSet();
    private AnimatorSet.Builder builder;
    private Runnable delayedStartRunnable;
    private List<Runnable> beforeRunnables = new ArrayList<>();
    private List<Runnable> duringRunnables = new ArrayList<>();
    private List<Runnable> afterRunnables = new ArrayList<>();
    private List<Runnable> insteadRunnables = new ArrayList<>();
    private TimeInterpolator actualInterpolator;
    private AnimationCluster nextCluster;
    private List<AnimationCluster> boundClusters = new ArrayList<>();
    private AnimationCluster bindingCluster;
    private long startDelay;
    private boolean finished;

    private AnimationCluster() {
        animatorSet.addListener(this);
        animatorSet.setInterpolator(this);
        mainHandler = new Handler(Looper.getMainLooper());
    }

    public static AnimationCluster create() {
        return new AnimationCluster();
    }

    /**
     * use this method to create a runnable that will set a view to GONE
     * - for chaining purposes
     *
     * @param view the view to set to GONE
     * @return the runnable created
     */
    public static Runnable setGone(final View view) {
        return new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.GONE);
            }
        };
    }

    /**
     * use this method to create a runnable that will set a view to VISIBLE
     * - for chaining purposes
     *
     * @param view the view to set to VISIBLE
     * @return the runnable created
     */
    public static Runnable setVisible(final View view) {
        return new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        };
    }

    /**
     * Try all manner of things to get a view height including making the view briefly VISIBLE and
     * force measuring it
     *
     * @param view the view to be measured
     * @return
     */
    private static int forceGetViewHeight(View view) {
        Integer height = (Integer) view.getTag(13);

        if (null == height) {
            height = view.getHeight();
        }

        if (height == 0) {
            //retain view visibility state
            int oldVisibility = view.getVisibility();

            //make view visible and force it to measure itself
            view.setVisibility(View.VISIBLE);
            view.measure(0, 0);
            //get the measured height
            height = view.getMeasuredHeight();

            //put the original visibility back
            view.setVisibility(oldVisibility);
        }

        return height;
    }

    /**
     * Set the layout height of a view to a certain value
     * - this method will store the original height in a tag to make it available
     * in the future if the view is to be set back to its original height
     *
     * @param view   the view to be changed
     * @param height the height to be set on the view
     */
    private static void forceSetViewHeight(View view, int height) {
        if (null == view.getTag(13)) {
            view.setTag(13, forceGetViewHeight(view));
        }
        view.getLayoutParams().height = height;
        view.requestLayout();
    }

    /**
     * ********************************************************************************************
     * ********************************************************************************************
     * ********************************************************************************************
     * ********************************************************************************************
     * ********************************************************************************************
     * BELOW ARE THE ANIMATION METHODS
     */

    private void setBindingCluster(AnimationCluster bindingCluster) {
        this.bindingCluster = bindingCluster;
    }

    /**
     * Chain this animation cluster with another, NOTE : an animation cluster can only be chained with the next link in the chain
     * multiple calls to chain on the same instance will cause a RuntimeException
     *
     * @param nextCluster the cluster to be run after this one is finished
     * @return this cluster
     */
    public AnimationCluster chain(AnimationCluster nextCluster) {
        if (nextCluster != null) {
            throw new RuntimeException("AnimationCluster can only be chained once");
        }
        this.nextCluster = nextCluster;
        nextCluster.setBindingCluster(this);
        return this;
    }

    /**
     * Bind this animation cluster with another
     *
     * @param boundCluster the cluster to be run in parallel with this one
     * @return this cluster
     */
    public AnimationCluster bind(AnimationCluster boundCluster) {
        boundClusters.add(boundCluster);
        boundCluster.setBindingCluster(this);
        return this;
    }

    @Override
    public float getInterpolation(float input) {
        onAnimationStep();
        return (actualInterpolator == null) ? input : actualInterpolator.getInterpolation(input);
    }

    /**
     * Add an animator to be played in this cluster
     *
     * @param animator the animator to be played
     * @return this cluster
     */
    private AnimationCluster play(Animator animator) {
        if (builder == null) {
            builder = animatorSet.play(animator);
        } else {
            builder.with(animator);
        }

        return this;
    }

    /**
     * Roll the view up from the current height to 0
     *
     * @param view     the target view
     * @param duration the animation duration
     */
    public AnimationCluster slideUp(View view, long duration) {
        int height = forceGetViewHeight(view);
        play(ObjectAnimator.ofFloat(view, "translationY", height, 0).setDuration(duration));
        return this;
    }

    /**
     * Roll the view up from the current height to 0
     *
     * @param view     the target view
     * @param duration the animation duration
     */
    public AnimationCluster slideDown(View view, long duration) {
        int height = forceGetViewHeight(view);
        play(ObjectAnimator.ofFloat(view, "translationY", 0, height).setDuration(duration));
        return this;
    }

    /**
     * Roll the view up from the current height to 0
     *
     * @param view     the target view
     * @param duration the animation duration
     */
    public AnimationCluster blindUp(View view, long duration) {
        int height = forceGetViewHeight(view);
        ValueAnimator animator = ValueAnimator.ofInt(height, 0).setDuration(duration);
        animator.addUpdateListener(new LayoutHeightUpdater(view));
        play(animator);

        return this;
    }

    /**
     * Roll the view up from the current height to 0
     *
     * @param view     the target view
     * @param duration the animation duration
     */
    public AnimationCluster blindDown(View view, long duration) {
        int height = forceGetViewHeight(view);
        ValueAnimator animator = ValueAnimator.ofInt(0, height).setDuration(duration);
        animator.addUpdateListener(new LayoutHeightUpdater(view));
        play(animator);

        return this;
    }

    /**
     * grow the view in from scale 0.1 to scale 1
     *
     * @param view     the target text view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster grow(View view, long duration) {
        play(ObjectAnimator.ofFloat(view, "scaleX", 0.1f, 1f).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "scaleY", 0.1f, 1f).setDuration(duration));
        return this;
    }

    /**
     * shrink the view out from scale 1 to scale 0.1
     *
     * @param view     the target text view
     * @param duration the animation duration
     */
    public AnimationCluster shrink(View view, long duration) {
        play(ObjectAnimator.ofFloat(view, "scaleX", 1f, 0.1f).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "scaleY", 1f, 0.1f).setDuration(duration));
        return this;
    }

    /**
     * fade the view in from alpha 0 to alpha 1
     *
     * @param view     the target text view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster fadeIn(View view, long duration) {
        play(ObjectAnimator.ofFloat(view, "alpha", 0f, 1f).setDuration(duration));
        return this;
    }

    /**
     * fade the view out from alpha 1 to alpha 0
     *
     * @param view     the target text view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster fadeOut(View view, long duration) {
        play(ObjectAnimator.ofFloat(view, "alpha", 1f, 0f).setDuration(duration));
        return this;
    }

    /**
     * make the text size grow from 0 to current text size
     *
     * @param textView the target text view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster textGrow(TextView textView, long duration) {
        ValueAnimator animator = ValueAnimator.ofFloat(0, textView.getTextSize()).setDuration(duration);
        animator.addUpdateListener(new TextSizeUpdater(textView));
        play(animator);
        return this;
    }

    /**
     * make the text size shrink from the current text size to 0
     *
     * @param textView the target text view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster textShrink(TextView textView, long duration) {
        ValueAnimator animator = ValueAnimator.ofFloat(textView.getTextSize(), 0).setDuration(duration);
        animator.addUpdateListener(new TextSizeUpdater(textView));
        play(animator);
        return this;
    }

    /**
     * rotate a view along the x axis
     *
     * @param view        the target view
     * @param duration    the animation duration
     * @param fromDegrees the degrees of rotation to start from
     * @param toDegrees   the degrees of rotation to end on
     * @return this cluster
     */
    public AnimationCluster rotateX(View view, long duration, float fromDegrees, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotationX", fromDegrees, toDegrees).setDuration(duration));
    }

    public AnimationCluster rotateX(View view, long duration, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotationX", toDegrees).setDuration(duration));
    }

    /**
     * rotate a view along the y axis
     *
     * @param view        the target view
     * @param duration    the animation duration
     * @param fromDegrees the degrees of rotation to start from
     * @param toDegrees   the degrees of rotation to end on
     * @return this cluster
     */
    public AnimationCluster rotateY(View view, long duration, float fromDegrees, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotationY", fromDegrees, toDegrees).setDuration(duration));
    }

    public AnimationCluster rotateY(View view, long duration, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotationY", toDegrees).setDuration(duration));
    }

    /**
     * rotate a view along the z axis
     *
     * @param view        the target view
     * @param duration    the animation duration
     * @param fromDegrees the degrees of rotation to start from
     * @param toDegrees   the degrees of rotation to end on
     * @return this cluster
     */
    public AnimationCluster rotate(View view, long duration, float fromDegrees, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotation", fromDegrees, toDegrees).setDuration(duration));
    }

    public AnimationCluster rotate(View view, long duration, float toDegrees) {
        return play(ObjectAnimator.ofFloat(view, "rotation", toDegrees).setDuration(duration));
    }

    /**
     * translate a view along the x axis
     *
     * @param view     the target view
     * @param duration the animation duration
     * @param x        the value to translate to on x axis
     * @return this cluster
     */
    public AnimationCluster translateX(View view, long duration, float x) {
        return play(ObjectAnimator.ofFloat(view, "translationX", x).setDuration(duration));
    }

    /**
     * translate a view along the y axis
     *
     * @param view     the target view
     * @param duration the animation duration
     * @param y        the value to translate to on y axis
     * @return this cluster
     */
    public AnimationCluster translateY(View view, long duration, float y) {
        return play(ObjectAnimator.ofFloat(view, "translationY", y).setDuration(duration));
    }

    /**
     * translate a view along the x, y axis
     *
     * @param view     the target view
     * @param duration the animation duration
     * @param x        the value to translate to on x axis
     * @param y        the value to translate to on y axis
     * @return this cluster
     */
    public AnimationCluster translate(View view, long duration, float x, float y) {
        return play(ObjectAnimator.ofFloat(view, "translationX", x).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "translationY", y).setDuration(duration));
    }

    public AnimationCluster translate(View view, long duration, float fromX, float fromY, float toX, float toY) {
        return play(ObjectAnimator.ofFloat(view, "translationX", fromX, toX).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "translationY", fromY, toY).setDuration(duration));
    }

    /**
     * ONLY USED FOR CUSTOM VIEWS !!!!
     * scale a custom view
     *
     * @param view     the target view
     * @param duration the animation duration
     * @return this cluster
     */
    public AnimationCluster scale(View view, long duration, float from, float to) {
        return play(ObjectAnimator.ofFloat(view, "scale", from, to).setDuration(duration));
    }

    public AnimationCluster scale(View view, long duration, float to) {
        return play(ObjectAnimator.ofFloat(view, "scale", to).setDuration(duration));
    }

    /**
     * scale a view along its x, y axis
     *
     * @param view     the target view
     * @param duration the animation duration
     * @param fromX    the value to scale from on x axis
     * @param fromY    the value to scale from on y axis
     * @param toX      the value to scale to on x axis
     * @param toY      the value to scale to on y axis
     * @return this cluster
     */
    public AnimationCluster scaleXY(View view, long duration, float fromX, float fromY, float toX, float toY) {
        return play(ObjectAnimator.ofFloat(view, "scaleX", fromX, toX).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "scaleY", fromY, toY).setDuration(duration));
    }

    public AnimationCluster scaleXY(View view, long duration, float toX, float toY) {
        return play(ObjectAnimator.ofFloat(view, "scaleX", toX).setDuration(duration))
                .play(ObjectAnimator.ofFloat(view, "scaleY", toY).setDuration(duration));
    }

    /**
     * Add a runnable to be executed before the animation starts
     *
     * @param runnable the runnable to be executed
     * @return this cluster
     */
    public AnimationCluster before(Runnable runnable) {
        if (!beforeRunnables.contains(runnable)) {
            beforeRunnables.add(runnable);
        }
        return this;
    }

    /**
     * Add a runnable to be executed before the animation starts
     *
     * @param runnable the runnable to be executed
     * @return this cluster
     */
    public AnimationCluster during(Runnable runnable) {
        if (!duringRunnables.contains(runnable)) {
            duringRunnables.add(runnable);
        }
        return this;
    }

    /**
     * delay the start of the animation
     *
     * @param startDelay
     * @return
     */
    public AnimationCluster delay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    /**
     * Add a runnable to be executed after the animation ends
     *
     * @param runnable the runnable to be executed
     * @return this cluster
     */
    public AnimationCluster after(Runnable runnable) {
        if (!afterRunnables.contains(runnable)) {
            afterRunnables.add(runnable);
        }
        return this;
    }

    /**
     * Add a runnable to be executed instead of the animation (on cancel)
     *
     * @param runnable the runnable to be executed
     * @return this cluster
     */
    public AnimationCluster instead(Runnable runnable) {
        if (!insteadRunnables.contains(runnable)) {
            insteadRunnables.add(runnable);
        }
        return this;
    }

    /**
     * Add a custom interpolator to the animation cluster
     *
     * @param interpolator the interpolator being added
     * @return this cluster
     */
    public AnimationCluster setInterpolator(TimeInterpolator interpolator) {
        this.actualInterpolator = interpolator;
        return this;
    }

    public boolean isEmplty() {
        return animatorSet.getChildAnimations().size() == 0;
    }

    public boolean isFinished() {
        for (AnimationCluster boundCluster : boundClusters) {
            if (!boundCluster.isFinished()) {
                return false;
            }
        }
        return (nextCluster == null) ? finished : nextCluster.isFinished() && finished;
    }

    /**
     * Start the animation cluster instantly or with a delay
     */
    public AnimationCluster start() {
        //start this cluster (or schedule start after delay)
        if (startDelay == 0) {
            animatorSet.start();
        } else {
            mainHandler.postDelayed(getDelayedStartRunnable(), startDelay);
        }

        //start all bound clusters
        for (AnimationCluster boundCluster : boundClusters) {
            boundCluster.start();
        }

        return this;
    }

    /**
     * cancel the animation
     */
    public void cancel() {
        mainHandler.removeCallbacks(delayedStartRunnable);
        animatorSet.cancel();
    }

    @Override
    public void onAnimationStart(Animator animation) {
        for (Runnable beforeRunnable : beforeRunnables) {
            beforeRunnable.run();
        }
    }

    public void onAnimationStep() {
        for (Runnable duringRunnable : duringRunnables) {
            duringRunnable.run();
        }
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        finished = true;

        if (nextCluster != null) {
            nextCluster.start();
        }
        if (isFinished()) {
            for (Runnable afterRunnable : afterRunnables) {
                afterRunnable.run();
            }
        }

        if (bindingCluster != null) {
            bindingCluster.onAnimationEnd(animation);
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        finished = true;
        if (nextCluster != null) {
            nextCluster.cancel();
        }
        if (isFinished()) {
            for (Runnable insteadRunnable : insteadRunnables) {
                insteadRunnable.run();
            }
        }

        if (bindingCluster != null) {
            bindingCluster.onAnimationCancel(animation);
        }
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        //NOP
    }

    public Runnable getDelayedStartRunnable() {
        if (delayedStartRunnable == null) {
            delayedStartRunnable = new Runnable() {
                @Override
                public void run() {
                    animatorSet.start();
                }
            };
        }
        return delayedStartRunnable;
    }

    /**
     * this will update the height of a view during animation
     */
    private static class LayoutHeightUpdater implements ValueAnimator.AnimatorUpdateListener {
        private View target;

        private LayoutHeightUpdater(View target) {
            this.target = target;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            forceSetViewHeight(target, (Integer) animation.getAnimatedValue());
        }
    }

    /**
     * this will update the text size of a text view during animation
     */
    private static class TextSizeUpdater implements ValueAnimator.AnimatorUpdateListener {
        private final TextView textView;

        public TextSizeUpdater(TextView textView) {
            this.textView = textView;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (Float) animation.getAnimatedValue());
        }
    }

    /**
     * This is a custom spring interpolator (similar to bounce but it is linear in bow up and down bounce movements
     * so it is more like a stabilizing spring
     */
    public static class SpringInterpolator implements TimeInterpolator {
        private float noSpringQuota;
        private float oneSpringQuota;
        private float springAmplitude;
        private float amplitudeDecreaseFactor;

        public SpringInterpolator(float springQuota, float springAmplitude, float amplitudeDecreaseFactor, int springRepetitions) {
            this.noSpringQuota = 1 - springQuota;
            this.springAmplitude = springAmplitude;
            this.amplitudeDecreaseFactor = amplitudeDecreaseFactor;
            this.oneSpringQuota = springQuota / springRepetitions;
        }

        public float getInterpolation(float t) {
            if (t < noSpringQuota) {
                return t / noSpringQuota;
            } else {
                double oscillationOffset = t - noSpringQuota;
                int completedOscillations = (int) (oscillationOffset / oneSpringQuota);
                double currentWavePosition = oscillationOffset - oneSpringQuota * completedOscillations;
                double currentWaveDegrees = currentWavePosition * 2 * Math.PI / oneSpringQuota;
                double currentWaveSin = Math.sin(currentWaveDegrees);
                return (float) (1 + currentWaveSin * springAmplitude / (completedOscillations * amplitudeDecreaseFactor + 1));
            }
        }
    }
}
