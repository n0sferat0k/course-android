package com.lorand.lesson.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lorand.lesson.R;
import com.lorand.lesson.bean.LogItem;
import com.lorand.lesson.bl.CarController;

import java.util.ArrayList;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class MoveListAdapter extends BaseAdapter implements CarController.CommandLogger {
    private ArrayList<LogItem> logItems = new ArrayList<LogItem>();

    @Override
    public int getCount() {
        return logItems.size();
    }

    @Override
    public LogItem getItem(int position) {
        return logItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LogItemHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.move_list_item, null);
            holder = new LogItemHolder(
                    (ImageView) convertView.findViewById(R.id.direction_arrow),
                    (ImageView) convertView.findViewById(R.id.direction_none),
                    (TextView) convertView.findViewById(R.id.move_name),
                    (TextView) convertView.findViewById(R.id.move_duration)
            );
            convertView.setTag(holder);
        } else {
            holder = (LogItemHolder) convertView.getTag();
        }

        LogItem item = getItem(position);
        if (item.getDirDeg() == null) {
            holder.directionArrow.setVisibility(View.GONE);
            holder.directionNone.setVisibility(View.VISIBLE);
        } else {
            holder.directionArrow.setVisibility(View.VISIBLE);
            holder.directionNone.setVisibility(View.GONE);

            holder.directionArrow.setRotation(item.getDirDeg());
        }

        holder.moveName.setText(item.getName());
        holder.moveDuration.setText(parent.getResources().getString(R.string.duration_format, item.getDuration()));

        return convertView;
    }

    public ArrayList<LogItem> getLogItems() {
        return logItems;
    }

    public void setLogItems(ArrayList<LogItem> logItems) {
        this.logItems = logItems;
        notifyDataSetChanged();
    }

    @Override
    public void logCommand(Integer dirDeg, String name, long duration) {
        logItems.add(new LogItem(dirDeg, name, duration));
        notifyDataSetChanged();
    }

    private class LogItemHolder {
        private ImageView directionArrow;
        private ImageView directionNone;
        private TextView moveName;
        private TextView moveDuration;

        public LogItemHolder(ImageView directionArrow, ImageView directionNone, TextView moveName, TextView moveDuration) {
            this.directionArrow = directionArrow;
            this.directionNone = directionNone;
            this.moveName = moveName;
            this.moveDuration = moveDuration;
        }
    }
}
