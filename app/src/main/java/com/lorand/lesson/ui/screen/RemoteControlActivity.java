package com.lorand.lesson.ui.screen;

import android.app.Activity;
import android.os.Bundle;

import com.lorand.lesson.R;
import com.lorand.lesson.bl.CarController;
import com.lorand.lesson.bl.TcpIpConnectionManager;
import com.lorand.lesson.ui.fragment.JoystickFragment;
import com.lorand.lesson.ui.fragment.MoveListFragment;

/**
 * Created by n0sferat0k on 1/6/2016.
 */
public class RemoteControlActivity extends Activity {

    private CarController carController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remote_control_activity);

        carController = new CarController();
        carController.setCommandSender(new CarController.CommandSender() {
            @Override
            public void send(String command) {
                TcpIpConnectionManager.getInstance().send(command);
            }
        });

        ((JoystickFragment) getFragmentManager().findFragmentById(R.id.joystick_fragment)).setCarController(carController);
        ((MoveListFragment) getFragmentManager().findFragmentById(R.id.move_list_fragment)).setCarController(carController);
    }
}
