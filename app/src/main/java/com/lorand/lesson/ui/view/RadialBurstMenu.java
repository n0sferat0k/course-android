package com.lorand.lesson.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.lorand.lesson.R;
import com.lorand.lesson.ui.util.AnimationCluster;

public class RadialBurstMenu extends RelativeLayout implements ViewGroup.OnHierarchyChangeListener, View.OnTouchListener {
    private static final long SELECTION_GROW_ANIMATION_DURATION_MS = 100;
    private static final long BURST_ANIMATION_DURATION_MS = 200;
    private static final long BURST_ANIMATION_FANOUT_DELAY_MS = 20;
    public static final int DEFAULT_FROM_DEGREES = 180;
    public static final int DEFAULT_TO_DEGREES = 0;
    public static final int DEFAULT_RADIUS = 100;
    private static final float DEFAULT_GROWTH_FACTOR = 1.5f;

    private int radius = DEFAULT_RADIUS;
    private int fromDegrees = DEFAULT_FROM_DEGREES;
    private int toDegrees = DEFAULT_TO_DEGREES;
    private OrigoGravity origoGravity = OrigoGravity.TOP_LEFT;
    private int origoAdjustX;
    private int origoAdjustY;
    private float selectionGrowthFactor;

    private int lastMeasuredHeight;
    private int lastMeasuredWidth;

    private View lastFocusedChild;

    private AnimationCluster lastAnimation;
    private Point origo;

    public RadialBurstMenu(Context context) {
        super(context);
        init(context, null);
    }

    public RadialBurstMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RadialBurstMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * *******************************************************************************************
     * Initializations
     */

    private void init(Context context, AttributeSet attrs) {
        setFocusable(true);
        setFocusableInTouchMode(true);

        if (attrs != null) {
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RadialBurstMenu, 0, 0);
            try {
                radius = (int) typedArray.getDimension(R.styleable.RadialBurstMenu_radius, DEFAULT_RADIUS);
                fromDegrees = typedArray.getInteger(R.styleable.RadialBurstMenu_fromDegrees, DEFAULT_FROM_DEGREES);
                toDegrees = typedArray.getInteger(R.styleable.RadialBurstMenu_toDegrees, DEFAULT_TO_DEGREES);
                origoGravity = OrigoGravity.getById(typedArray.getInteger(R.styleable.RadialBurstMenu_origoGravity, OrigoGravity.TOP_LEFT.getId()));
                origoAdjustX = (int) typedArray.getDimension(R.styleable.RadialBurstMenu_origoAdjustX, 0);
                origoAdjustY = (int) typedArray.getDimension(R.styleable.RadialBurstMenu_origoAdjustY, 0);
                selectionGrowthFactor = typedArray.getFloat(R.styleable.RadialBurstMenu_selectionGrowthFactor, DEFAULT_GROWTH_FACTOR);
            } finally {
                typedArray.recycle();
            }
        }
        setOnHierarchyChangeListener(this);
    }

    /**
     * *******************************************************************************************
     * Public state getters / exporters
     */

    /**
     * *******************************************************************************************
     * shielded functionality methods. USE CAUTION WHEN CALLING/OVERRIDING THESE !!!
     */

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();

        if (measuredWidth != this.lastMeasuredWidth || measuredHeight != this.lastMeasuredHeight) {
            this.lastMeasuredWidth = measuredWidth;
            this.lastMeasuredHeight = measuredHeight;
            reArrangeChildren();
        }
    }

    @Override
    public void onChildViewAdded(View parent, View child) {
        reArrangeChildren();
    }

    @Override
    public void onChildViewRemoved(View parent, View child) {
        reArrangeChildren();
    }

    /**
     * This method will arrange all children in a radial fashion
     */
    private void reArrangeChildren() {
        int count = getChildCount();
        int x, y;

        //do nothing if we have no size or not enough children
        if (lastMeasuredWidth == 0 || lastMeasuredHeight == 0 || count < 3) {
            return;
        }

        origo = origoGravity.getOrigo(lastMeasuredWidth, lastMeasuredHeight, origoAdjustX, origoAdjustY);
        View centerChild = getChildAt(0);
        centerChild.setOnTouchListener(this);
        positionChildViaCenter(centerChild, origo.x, origo.y);

        for (int i = 1; i < count; i++) {
            View radialChild = getChildAt(i);
            radialChild.setVisibility(INVISIBLE);
            //below we work with i-1 instead of i because the 0'th child is in the center
            //below we work with count-2 instead of count because
            //  - the 0'th child is in the center
            //  - N children arranged in an arc delimit N - 1 equal degree slices (like a pie) between them
            double childAngle = Math.toRadians(fromDegrees + (toDegrees - fromDegrees) * (i - 1) / (count - 2));

            x = origo.x + (int) (radius * Math.cos(childAngle));
            y = origo.y - (int) (radius * Math.sin(childAngle));

            positionChildViaCenter(radialChild, x, y);
        }
    }

    private void positionChildViaCenter(View child, int x, int y) {
        int childStableX = x - child.getMeasuredWidth() / 2;
        int childStableY = y - child.getMeasuredHeight() / 2;

        //the below difference in signs is because while the view X is from left to right just like in the cartesian system of Math,
        //the y is from top to bottom, not bottom to top as in cartesian Math (thats why "origo.y - y" instead of "origo.y + y"
        LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();
        layoutParams.leftMargin = childStableX;
        layoutParams.topMargin = childStableY;
        child.requestLayout();

        child.setTag(1, childStableX);
        child.setTag(2, childStableY);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //if this is the center child
        if (v.equals(getChildAt(0))) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (!radialBurst(true, false)) {
                        return false;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (lastFocusedChild != null) {
                        lastFocusedChild.setSelected(false);
                        AnimationCluster.create().scaleXY(lastFocusedChild, SELECTION_GROW_ANIMATION_DURATION_MS, 1f, 1f).start();
                        lastFocusedChild.performClick();
                        lastFocusedChild = null;
                    }
                    //fall through on purpose
                case MotionEvent.ACTION_CANCEL:
                    radialBurst(false, true);
                    break;
                case MotionEvent.ACTION_MOVE:
                    int count = getChildCount();
                    boolean found = false;
                    View centerChild = getChildAt(0);
                    for (int i = 1; i < count; i++) {
                        View radialChild = getChildAt(i);
                        Rect rect = new Rect(radialChild.getLeft(), radialChild.getTop(), radialChild.getRight(), radialChild.getBottom());
                        if (rect.contains((int) event.getX() + centerChild.getLeft(), (int) event.getY() + centerChild.getTop())) {
                            found = true;
                            if (lastFocusedChild != radialChild) {
                                lastFocusedChild = radialChild;
                                lastFocusedChild.setSelected(true);
                                AnimationCluster.create().scaleXY(radialChild, SELECTION_GROW_ANIMATION_DURATION_MS, selectionGrowthFactor, selectionGrowthFactor).start();
                            }
                        } else {
                            if (radialChild.isSelected()) {
                                radialChild.setSelected(false);
                                AnimationCluster.create().scaleXY(radialChild, SELECTION_GROW_ANIMATION_DURATION_MS, 1f, 1f).start();
                            }
                        }
                    }
                    if (!found) {
                        lastFocusedChild = null;
                    }
                    break;
            }
        }

        return true;
    }

    /**
     * perform a radial burst animation in forward or backward direction
     *
     * @param forward true if forward burst
     */
    private boolean radialBurst(boolean forward, boolean force) {
        if (lastAnimation != null) {
            if (force) {
                lastAnimation.cancel();
                lastAnimation = null;
            } else {
                return false;
            }
        }

        lastAnimation = AnimationCluster.create();
        lastAnimation.setInterpolator(new DecelerateInterpolator());
        lastAnimation.after(new Runnable() {
            @Override
            public void run() {
                lastAnimation = null;
            }
        });

        final int count = getChildCount();
        long progressiveDelay = 0;

        //if we are bursting forward
        if (forward) {
            //add an animation for each child that will translate the child from the origo to the child stable position
            for (int i = 1; i < count; i++) {
                View radialChild = getChildAt(i);
                int stableX = (int) radialChild.getTag(1);
                int stableY = (int) radialChild.getTag(2);

                lastAnimation.bind(AnimationCluster.create()
                        .translate(radialChild, BURST_ANIMATION_DURATION_MS, origo.x - stableX - radialChild.getMeasuredWidth() / 2, origo.y - stableY - radialChild.getMeasuredHeight() / 2, 0, 0)
                        .fadeIn(radialChild, BURST_ANIMATION_DURATION_MS)
                        .delay(progressiveDelay));

                progressiveDelay += BURST_ANIMATION_FANOUT_DELAY_MS;
            }
            //then before running the animation make all the children visible but transparent
            lastAnimation.before(new Runnable() {
                @Override
                public void run() {
                    for (int i = 1; i < count; i++) {
                        View radialChild = getChildAt(i);
                        radialChild.setVisibility(VISIBLE);
                        radialChild.setAlpha(0);
                    }
                }
            });
            //if we are bursting backward
        } else {
            //add an animation for each child that will translate the child from its stable position to the origo
            for (int i = 1; i < count; i++) {
                View radialChild = getChildAt(i);
                int stableX = (int) radialChild.getTag(1);
                int stableY = (int) radialChild.getTag(2);

                lastAnimation.bind(AnimationCluster.create()
                        .translate(radialChild, BURST_ANIMATION_DURATION_MS, 0, 0, origo.x - stableX - radialChild.getMeasuredWidth() / 2, origo.y - stableY - radialChild.getMeasuredHeight() / 2)
                        .fadeOut(radialChild, BURST_ANIMATION_DURATION_MS)
                        .delay(progressiveDelay));

                progressiveDelay += BURST_ANIMATION_FANOUT_DELAY_MS;
            }
            //then after running the animation make all the childred invisible
            lastAnimation.after(new Runnable() {
                @Override
                public void run() {
                    for (int i = 1; i < count; i++) {
                        View radialChild = getChildAt(i);
                        radialChild.setVisibility(INVISIBLE);
                    }
                }
            });
        }

        lastAnimation.start();
        return true;
    }

    /**
     * *******************************************************************************************
     * Inner classes
     */

    private enum OrigoGravity {
        TOP_LEFT(0, 0f, 0f),
        TOP_CENTER(1, .5f, 0f),
        TOP_RIGHT(2, 1f, 0f),
        MIDDLE_LEFT(3, 0f, .5f),
        MIDDLE_CENTER(4, .5f, .5f),
        MIDDLE_RIGHT(5, 1f, .5f),
        BOTTOM_LEFT(6, 0f, 1f),
        BOTTOM_CENTER(7, .5f, 1f),
        BOTTOM_RIGHT(8, 1f, 1f);

        private int id;
        private final float origoPercentX;
        private final float origoPercentY;

        OrigoGravity(int id, float origoPercentX, float origoPercentY) {
            this.id = id;
            this.origoPercentX = origoPercentX;
            this.origoPercentY = origoPercentY;
        }

        public int getId() {
            return id;
        }

        public static OrigoGravity getById(int id) {
            for (OrigoGravity origoGravity : values()) {
                if (origoGravity.id == id) {
                    return origoGravity;
                }
            }
            return TOP_LEFT;
        }

        public Point getOrigo(int width, int height, int origoAdjustX, int origoAdjustY) {
            return new Point((int) (width * origoPercentX + origoAdjustX), (int) (height * origoPercentY + origoAdjustY));
        }
    }
}
