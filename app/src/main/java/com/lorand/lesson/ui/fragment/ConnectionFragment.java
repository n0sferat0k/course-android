package com.lorand.lesson.ui.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import com.lorand.lesson.R;
import com.lorand.lesson.bl.TcpIpConnectionManager;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class ConnectionFragment extends Fragment implements View.OnClickListener, TcpIpConnectionManager.TcpIpConnectionListener {
    public static final String CONNECTION_SETTINGS_SHARED_PREFS = "connection.settings.shared.prefs";
    private static final String IP_SP_KEY = "ip.shared.pref.key";
    private static final String PORT_SP_KEY = "port.shared.pref.key";

    private View connectionStateView;
    private EditText ip;
    private EditText port;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.connection_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        connectionStateView = getView().findViewById(R.id.connection_state);

        getView().findViewById(R.id.connect_button).setOnClickListener(this);
        getView().findViewById(R.id.disconnect_button).setOnClickListener(this);

        ip = (EditText) getView().findViewById(R.id.et_ip);
        port = (EditText) getView().findViewById(R.id.et_port);
    }

    @Override
    public void onResume() {
        super.onResume();
        TcpIpConnectionManager.getInstance().addTcpIpConnectionListener(this);

        connectionStateView.setEnabled(false);
        connectionStateView.setSelected(TcpIpConnectionManager.getInstance().isConnected());

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(CONNECTION_SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE);
        ip.setText(sharedPreferences.getString(IP_SP_KEY, ""));
        port.setText(sharedPreferences.getString(PORT_SP_KEY, ""));
    }

    @Override
    public void onPause() {
        super.onPause();
        TcpIpConnectionManager.getInstance().removeTcpIpConnectionListener(this);

        SharedPreferences.Editor editor = getActivity().getSharedPreferences(CONNECTION_SETTINGS_SHARED_PREFS, Context.MODE_PRIVATE).edit();
        editor.putString(IP_SP_KEY, ip.getText().toString());
        editor.putString(PORT_SP_KEY, port.getText().toString());
        editor.commit();
    }

    @Override
    public void onClick(View v) {
        ip.setError(null);
        port.setError(null);

        switch (v.getId()) {
            case R.id.connect_button:
                if (TextUtils.isEmpty(ip.getText().toString())) {
                    ip.setError(getString(R.string.provide_ip));
                    return;
                }
                if (!Patterns.IP_ADDRESS.matcher(ip.getText().toString()).matches()) {
                    ip.setError(getString(R.string.invalid_ip));
                    return;
                }
                if (TextUtils.isEmpty(port.getText().toString())) {
                    port.setError(getString(R.string.provide_port));
                    return;
                }
                try {
                    Integer.parseInt(port.getText().toString());
                } catch (NumberFormatException e) {
                    port.setError(getString(R.string.invalid_port));
                    return;
                }
                connectionStateView.setEnabled(true);
                connectionStateView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate));
                TcpIpConnectionManager.getInstance().connect(ip.getText().toString(), Integer.parseInt(port.getText().toString()));
                break;
            case R.id.disconnect_button:
                TcpIpConnectionManager.getInstance().disconnect();
                break;
        }
    }

    private void runOnUiThread(final Runnable runnable) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(runnable);
        }
    }

    @Override
    public void onStateChanged(final boolean connected) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectionStateView.clearAnimation();
                connectionStateView.setEnabled(false);
                connectionStateView.setSelected(connected);
            }
        });
    }

    @Override
    public void onConnectionError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectionStateView.clearAnimation();
                connectionStateView.setEnabled(false);
                connectionStateView.setSelected(false);
                ip.setError(getString(R.string.error_connecting));
            }
        });
    }
}
