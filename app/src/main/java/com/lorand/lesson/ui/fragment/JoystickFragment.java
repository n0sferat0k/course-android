package com.lorand.lesson.ui.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;

import com.lorand.lesson.R;
import com.lorand.lesson.bl.CarController;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class JoystickFragment extends Fragment implements View.OnTouchListener {

    private View knob;
    private float dragStartX;
    private float dragStartY;
    private CarController carController;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.joystick_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        knob = getView().findViewById(R.id.direction_knob);
        knob.setOnTouchListener(this);
    }

    public void setCarController(CarController carController) {
        this.carController = carController;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                dragStartX = event.getX();
                dragStartY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                int spaceX = ((ViewGroup) knob.getParent()).getWidth() / 2;
                int spaceY = ((ViewGroup) knob.getParent()).getHeight() / 2;

                int translateX = (int) (knob.getTranslationX() + event.getX() - dragStartX);
                int translateY = (int) (knob.getTranslationY() + event.getY() - dragStartY);

                carController.directionFromJoyStickPercent(translateX * 100 / spaceX, translateY * 100 / spaceY);

                knob.setTranslationX(Math.signum(translateX) * Math.min(Math.abs(translateX), Math.abs(spaceX - knob.getWidth() / 2)));
                knob.setTranslationY(Math.signum(translateY) * Math.min(Math.abs(translateY), Math.abs(spaceY - knob.getHeight() / 2)));
                break;
            case MotionEvent.ACTION_UP:
                carController.directionFromJoyStickPercent(0, 0);

                AnimatorSet set = new AnimatorSet();
                set.setInterpolator(new BounceInterpolator());
                set.setDuration(500);
                set.playTogether(
                        ObjectAnimator.ofFloat(knob, "translationX", knob.getTranslationX(), 0),
                        ObjectAnimator.ofFloat(knob, "translationY", knob.getTranslationY(), 0)
                );
                set.start();
                break;
        }
        return true;
    }
}
