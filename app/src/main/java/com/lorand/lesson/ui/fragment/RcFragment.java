package com.lorand.lesson.ui.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lorand.lesson.R;
import com.lorand.lesson.bl.CarController;
import com.lorand.lesson.bl.TcpIpConnectionManager;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class RcFragment extends Fragment {
    public static final String TAG = "RcFragment";
    private CarController carController;

    public RcFragment() {
        carController = new CarController();
        carController.setCommandSender(new CarController.CommandSender() {
            @Override
            public void send(String command) {
                TcpIpConnectionManager.getInstance().send(command);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rc_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((JoystickFragment) getChildFragmentManager().findFragmentById(R.id.joystick_fragment)).setCarController(carController);
        ((MoveListFragment) getChildFragmentManager().findFragmentById(R.id.move_list_fragment)).setCarController(carController);
    }
}
