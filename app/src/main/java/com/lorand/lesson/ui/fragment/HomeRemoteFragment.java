package com.lorand.lesson.ui.fragment;

import android.app.Fragment;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.lorand.lesson.R;

import java.io.FileReader;
import java.io.IOException;
import java.nio.CharBuffer;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class HomeRemoteFragment extends Fragment {
    public static final String TAG = "HomeRemoteFragment";

    private WebView remoteWebView;
    private WebView ipcamWebView;

    private ProgressBar remoteLoadingSpinner;
    private ProgressBar ipcamLoadingSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_remote_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        remoteLoadingSpinner = (ProgressBar)getView().findViewById(R.id.remote_loading_spinner);
        ipcamLoadingSpinner = (ProgressBar)getView().findViewById(R.id.ipcam_loading_spinner);

        remoteWebView = (WebView)getView().findViewById(R.id.remote_web_view);
        ipcamWebView = (WebView)getView().findViewById(R.id.ipcam_web_view);


        ipcamWebView.loadUrl("file:///android_asset/ipcam.html");
        ipcamWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ipcamLoadingSpinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                ipcamLoadingSpinner.setVisibility(View.GONE);
            }
        });

        remoteWebView.loadUrl("http://192.168.10.202");
        remoteWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                remoteLoadingSpinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                remoteLoadingSpinner.setVisibility(View.GONE);
            }
        });
    }
}
