package com.lorand.lesson.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class LogItem implements Parcelable {
    private Integer dirDeg;
    private String name;
    private long duration;

    public LogItem(Integer dirDeg, String name, long duration) {
        this.dirDeg = dirDeg;
        this.name = name;
        this.duration = duration;
    }

    public LogItem(Parcel in) {
        this.dirDeg = (Integer) in.readValue(ClassLoader.getSystemClassLoader());
        this.name = in.readString();
        this.duration = in.readLong();
    }

    public Integer getDirDeg() {
        return dirDeg;
    }

    public String getName() {
        return name;
    }

    public long getDuration() {
        return duration;
    }

    public static final Creator<LogItem> CREATOR = new Creator<LogItem>() {
        @Override
        public LogItem createFromParcel(Parcel in) {
            return new LogItem(in);
        }

        @Override
        public LogItem[] newArray(int size) {
            return new LogItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dirDeg);
        dest.writeString(name);
        dest.writeLong(duration);
    }
}
