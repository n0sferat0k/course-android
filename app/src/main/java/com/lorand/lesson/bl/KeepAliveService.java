package com.lorand.lesson.bl;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.lorand.lesson.R;

/**
 * Created by n0sferat0k on 1/10/2016.
 */
public class KeepAliveService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, getString(R.string.im_alive_too), Toast.LENGTH_LONG).show();
    }
}
