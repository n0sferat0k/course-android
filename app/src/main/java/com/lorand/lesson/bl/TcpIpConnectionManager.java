package com.lorand.lesson.bl;

import android.os.Handler;
import android.os.HandlerThread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TcpIpConnectionManager {
    private static final java.lang.String TAG = "TcpIpConnectionManager";

    private static TcpIpConnectionManager instance;

    private final HandlerThread handlerThread;
    private final Handler handler;

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;

    private List<TcpIpConnectionListener> listeners = new ArrayList<>();

    public TcpIpConnectionManager() {
        handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    public static TcpIpConnectionManager getInstance() {
        if (instance == null) {
            synchronized (TcpIpConnectionManager.class) {
                if (instance == null) {
                    instance = new TcpIpConnectionManager();
                }
            }
        }
        return instance;
    }

    public void connect(final String ip, final int port) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                innerConnect(ip, port);
            }
        });
    }

    private void innerConnect(String ip, int port) {
        innerDisconnect();

        try {
            socket = new Socket(ip, port);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            dispatchConnectionChnaged(true);
            startConnectionMonitoring();
            send("hello");
        } catch (IOException e) {
            dispatchConnectionError();
            innerDisconnect();
        }
    }

    private void startConnectionMonitoring() {
        new Thread(TAG) {
            @Override
            public void run() {
                try {
                    int read;
                    while (inputStream != null && (read = inputStream.read()) != -1) {
                        System.out.print((char) read);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    disconnect();
                }
            }
        }.start();
    }

    public void disconnect() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                innerDisconnect();
            }
        });
    }

    private void innerDisconnect() {
        if (socket != null && socket.isConnected()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                socket = null;
                inputStream = null;
                outputStream = null;

                dispatchConnectionChnaged(false);
            }
        }
    }

    public void send(String message) {
        try {
            if (outputStream != null) {
                outputStream.write(message.getBytes());
            }
        } catch (IOException e) {
            disconnect();
        }
    }

    private void dispatchConnectionError() {
        for (TcpIpConnectionListener listener : new ArrayList<>(listeners)) {
            listener.onConnectionError();
        }
    }

    private void dispatchConnectionChnaged(boolean connected) {
        for (TcpIpConnectionListener listener : new ArrayList<>(listeners)) {
            listener.onStateChanged(connected);
        }
    }

    public void addTcpIpConnectionListener(TcpIpConnectionListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeTcpIpConnectionListener(TcpIpConnectionListener listener) {
        while (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    public boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public interface TcpIpConnectionListener {
        void onStateChanged(boolean connected);

        void onConnectionError();
    }
}
