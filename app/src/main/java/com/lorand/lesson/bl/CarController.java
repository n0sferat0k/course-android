package com.lorand.lesson.bl;

/**
 * Created by n0sferat0k on 1/7/2016.
 */
public class CarController {
    public static final int THRESHOLD_PERCENT = 25;

    private long timestamp;

    private CommandSender commandSender;
    private CommandLogger commandLogger;

    private Direction direction = Direction.NONE;

    public CarController() {
        timestamp = System.currentTimeMillis();
    }

    /**
     * determine the car directions based on percentage of joystick movement
     *
     * @param percX -100 if joystick all the way to the left, 100 if joystick all the way to the right, 0 if joystick in middle on X
     * @param percY -100 if joystick all the way back, 100 if joystick all the way forward, 0 if joystick in middle on Y
     */
    public void directionFromJoyStickPercent(int percX, int percY) {
        int x = 0, y = 0;
        if (Math.abs(percX) > THRESHOLD_PERCENT) {
            x = (int) Math.signum(percX);
        }
        if (Math.abs(percY) > THRESHOLD_PERCENT) {
            y = (int) Math.signum(percY);
        }

        setDirection(Direction.getViaSigns(x, y));
    }

    public void setDirection(Direction direction) {
        if (this.direction != direction) {
            long duration = timestamp;
            timestamp = System.currentTimeMillis();
            duration = timestamp - duration;

            if (commandLogger != null) {
                commandLogger.logCommand(this.direction.dirDeg, this.direction.toString(), duration);
            }

            this.direction = direction;
            if (commandSender != null) {
                commandSender.send(direction.toCommand());
            }
        }
    }

    public void setCommandLogger(CommandLogger commandLogger) {
        this.commandLogger = commandLogger;
    }

    public void setCommandSender(CommandSender commandSender) {
        this.commandSender = commandSender;
    }

    private enum Direction {
        NONE(0, 0, "Idle", null),
        LEFT(-1, 0, "Idle left", 180),
        RIGHT(1, 0, "Idle right", 0),
        BACKWARD(0, 1, "Backward", 90),
        FORWARD(0, -1, "Forward", 270),
        LB(-1, 1, "Backward left ", 135),
        LF(-1, -1, "Forward left", 225),
        RB(1, 1, "Backward right", 45),
        RF(1, -1, "Forward right", 315);

        private final int signX;
        private final int signY;
        private final String name;
        private final Integer dirDeg;

        Direction(int signX, int signY, String name, Integer dirDeg) {
            this.signX = signX;
            this.signY = signY;
            this.name = name;
            this.dirDeg = dirDeg;
        }

        public static Direction getViaSigns(int x, int y) {
            for (Direction d : values()) {
                if ((d.signX == x) && (d.signY == y)) {
                    return d;
                }
            }
            return NONE;
        }

        @Override
        public String toString() {
            return name;
        }

        public String toCommand() {
            return
                    "pin=1 on=" + ((signX == -1) ? 1 : 0) + "\r\n" +
                            "pin=2 on=" + ((signX == 1) ? 1 : 0) + "\r\n" +
                            "pin=3 on=" + ((signY == -1) ? 1 : 0) + "\r\n" +
                            "pin=4 on=" + ((signY == 1) ? 1 : 0) + "\r\n";
        }
    }

    public interface CommandSender {
        public void send(String command);
    }

    public interface CommandLogger {
        void logCommand(Integer dirDeg, String name, long duration);
    }
}
