wifi.setmode(wifi.STATION)
wifi.sta.config("ANDROID_CRS","androidcourse")

states = {};

button_lookup = {"A", "B", "C", "D", "E"};
state_lookup = {"on", "off"}
cmd = {}

gpio.mode(2, gpio.OUTPUT);        
gpio.write(2, 0);

cfg =
{
ip="192.168.11.203",
netmask="255.255.255.0",
gateway="192.168.11.1"
}
wifi.sta.setip(cfg)

srv=net.createServer(net.TCP)

srv:listen(80,function(conn)
    conn:on("receive", function(client,request)                
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end
        local _GET = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                _GET[k] = v
            end
        end
        
        if (_GET.area ~= nil) then
            --first set up the area part of the message - number between 0 and 31
            area = tonumber(_GET.area);        
            for i = 1, 5, 1 do   
                mask = bit.lshift(1, i - 1);
                if bit.band(area, mask) == 0 then
                    table.insert(cmd, false);
                    table.insert(cmd, true);
                else
                    table.insert(cmd, true);
                    table.insert(cmd, true);
                end
            end
            
            --next set up the button group A,B,C,D,E or F
            button = _GET.button;
            for i = 1, 5, 1 do 
                if button_lookup[i] == button then
                    table.insert(cmd, false);
                    table.insert(cmd, false);
                else
                    table.insert(cmd, false);
                    table.insert(cmd, true);
                end            
            end
            
            --_GET.button
            --and finally the button within the group 1 or 2 (AKA on or off)

            state = _GET.state
            for i = 1, 2, 1 do 
                if state_lookup[i] == state then
                    table.insert(cmd, false);
                    table.insert(cmd, true);                
                else
                    table.insert(cmd, false);
                    table.insert(cmd, false);
                end
            end                
            
            --dont forget the stop bit
            table.insert(cmd, false);
    
            --the message is ready, send it to the RF transmitter (need to send it multiple times or the receiver will not react)
            cnt = table.getn(cmd);
            for i = 1, 5, 1 do                
                for i = 1, cnt, 1 do        
                    gpio.write(2, 1);       
                    if(cmd[i]) then
                    tmr.delay(200);
                    end
                    gpio.write(2, 0);                                   
                    if(not cmd[i]) then
                    tmr.delay(200);
                    end
                end 
                tmr.delay(4000);
            end

            --set the state variable to this new known value 
            if(states[area] == nil) then
                states[area] = {};
            end            
            states[area][button] = state;            

            --reset command variable
            cmd = {};
        end

        --output reply to client
        if path == "/json.api" then 
            client:send("{\"areas\":[");
            for area, buttons in pairs(states) do
              client:send("{\"area\":\""..area.."\", \"buttons\":[");
                for button, state in pairs(buttons) do  
                    client:send("{\"button\":\""..button.."\", \"state\":\""..state.."\"}");
                end                
              client:send("]}");
            end
            client:send("]}");
        else
            client:send("<h1> NodeMcu (RF light control) <br />");
            client:send(cfg.ip.."<br />");
            client:send("<a href=\"/json.api\">Json API</a><br />");
            client:send("</h1>");
            
            client:send("<span style=\"font-size:40px;\">");
    
            client:send("Area (0 - 31):<input style=\"font-size:40px;\" type=\"text\" id=\"area\" size=\"10\" value=\""..(area or 0).."\"/><br />");
    
            for i = 1, table.getn(button_lookup), 1 do  
                client:send("<div style=\"float:left;clear:both;margin:15px;\">");
                client:send("<button style=\"font-size:40px;margin-right:100px\" onclick=\"document.location='?area=' + document.getElementById('area').value + '&button="..button_lookup[i].."&state=on'\">ON</button>");
                client:send(button_lookup[i]);
                client:send("<button style=\"font-size:40px;margin-left:100px\" onclick=\"document.location='?area=' + document.getElementById('area').value + '&button="..button_lookup[i].."&state=off'\">OFF</button>");          
                client:send("</div>");
            end        

            client:send("</span>");
        end        
                
        client:close();
        collectgarbage();
    end)
end)
