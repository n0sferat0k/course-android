wifi.setmode(wifi.STATION)
wifi.sta.config("ANDROID_CRS","androidcourse")

gpios = {"GPIO 4", "GPIO 5", "GPIO 12", "GPIO 14"}
ports = {2, 1, 6, 5}
states = {false, false, false, false}

for i = 1, table.getn(ports), 1 do          
    gpio.mode(ports[i], gpio.OUTPUT);
    gpio.write(ports[i], states[i] and gpio.HIGH or gpio.LOW);
end

cfg =
{
ip="192.168.11.202",
netmask="255.255.255.0",
gateway="192.168.11.1"
}
wifi.sta.setip(cfg)

srv=net.createServer(net.TCP)

function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

srv:listen(80, function(conn)
    conn:on("receive", function(client, requests)                
        if requests ~= nil then
            requestTokens = split(requests,"\r?\n");
            for i = 1, table.getn(requestTokens), 1 do             
                request = requestTokens[i];
                print(request);        
                
                local command = {}        
                for k, v in string.gmatch(request, "(%w+)=(%w+)%s*") do
                    command[k] = v
                end
                                
                if command.pin then                     
                    for i = 1, table.getn(ports), 1 do             
                      if(command.pin == (""..i)) then
                           states[i] = command.on == "1";                                             
                           gpio.write(ports[i], states[i] and gpio.HIGH or gpio.LOW);
                      end
                    end            
                end
            end
        end    
    end)
end)
