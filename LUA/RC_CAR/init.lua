wifi.setmode(wifi.STATION)
wifi.sta.config("ANDROID_CRS","androidcourse")

lights_front = 2; 
lights_back = 1; 

gpio.mode(lights_front, gpio.OUTPUT);
gpio.write(lights_front, gpio.HIGH);
gpio.mode(lights_back, gpio.OUTPUT);
gpio.write(lights_back, gpio.HIGH);

dir_LR = 5;
dir_FB = 7;

gpio.mode(dir_LR, gpio.OUTPUT);
gpio.write(dir_LR, gpio.LOW);
gpio.mode(dir_FB, gpio.OUTPUT);
gpio.write(dir_FB, gpio.LOW);

int_LR = 6;
int_FB = 8;

pwm.setup(int_LR, 100, 0);
pwm.start(int_LR);
pwm.setup(int_FB, 100, 0);
pwm.start(int_FB);

cfg =
{
ip="192.168.11.201",
netmask="255.255.255.0",
gateway="192.168.11.1"
}
wifi.sta.setip(cfg)

srv=net.createServer(net.TCP)

function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

srv:listen(80, function(conn)
    conn:on("receive", function(client, requests)                
        if requests ~= nil then
            requestTokens = split(requests,"\r?\n");
            for i = 1, table.getn(requestTokens), 1 do             
                request = requestTokens[i];
                print(request);                                
                local command = {}  
                      
                for k, v in string.gmatch(request, "(%w+)=(%w+)%s*") do
                    command[k] = v
                end

                if command.turn == "left" then
                    gpio.write(dir_LR, gpio.HIGH);
                    pwm.setduty(int_LR, 1023 * math.min(command.turni, 100) / 100);
                end 
                if command.turn == "right" then
                    gpio.write(dir_LR, gpio.LOW);
                    pwm.setduty(int_LR, 1023 * math.min(command.turni, 100) / 100);
                end
                if command.turn == "none" then
                    gpio.write(dir_LR, gpio.LOW);
                    pwm.setduty(int_LR, 0);
                end
                
                if command.go == "forward" then
                    gpio.write(dir_FB, gpio.HIGH);
                    pwm.setduty(int_FB, 1023 * math.min(command.goi, 100) / 100);
                end 
                if command.go == "back" then
                    gpio.write(dir_FB, gpio.LOW);
                    pwm.setduty(int_FB, 1023 * math.min(command.goi, 100) / 100);
                end
                if command.go == "none" then
                    gpio.write(dir_FB, gpio.LOW);
                    pwm.setduty(int_FB, 0);
                end

                if command.lights == "front" then
                    gpio.write(lights_front, gpio.LOW);
                    gpio.write(lights_back, gpio.HIGH);
                end
                if command.lights == "back" then
                    gpio.write(lights_front, gpio.HIGH);
                    gpio.write(lights_back, gpio.LOW);
                end
                if command.lights == "both" then
                    gpio.write(lights_front, gpio.LOW);
                    gpio.write(lights_back, gpio.LOW);
                end
                if command.lights == "none" then
                    gpio.write(lights_front, gpio.HIGH);
                    gpio.write(lights_back, gpio.HIGH);
                end
            end
        end    
    end)
end)
